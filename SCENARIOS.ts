class SCENARIOS {
    static PHONE_1000 = "1000"
    static PHONE_1001 = "1001"
    static PHONE_1002 = "1002"
    static PHONE_1003 = "1003"
    static PHONE_1004 = "1004"
    static PHONE_1005 = "1005"
    static PHONE_1006 = "1006"
    static PHONE_1007 = "1007"
    static PHONE_1008 = "1008"

    static CREDENTIAL_2000 = "2000"
    static CREDENTIAL_2001 = "2001"
    static CREDENTIAL_2002 = "2002"
    static CREDENTIAL_2003 = "2003"
    static CREDENTIAL_2004 = "2004"
    static CREDENTIAL_2005 = "2005"
    static CREDENTIAL_2006 = "2006"
    static CREDENTIAL_2007 = "2007"
    static CREDENTIAL_2008 = "2008"
}

module.exports = {SCENARIOS}
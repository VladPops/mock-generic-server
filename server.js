var express = require('express')
var app = express()
var bodyParser = require('body-parser');
const { SCENARIOS } = require('./SCENARIOS.ts')
console.log(SCENARIOS.PHONE_1000)
const port = process.env.PORT || 3000
const INVALID = "invalid"
const red = "red"
const dis = "dis"
const reddis = "reddis"
var LAST_CREDENTIAL = "DEFAULT"
const { 
    VALIDATE_AGAINST,
    GENERIC_CODE_SUCCESS,
    GENERIC_CODE_NO_VALUE,
    GENERIC_CODE_ALREADY_REFUNDED,
    GENERIC_CODE_INSUFFICIENT_FUNDS,
    GENERIC_CODE_CARD_ALREADY_ACTIVE,
    GENERIC_CODE_CARD_NOT_ACTIVE,
    GENERIC_CODE_INVALID_CREDENTIALS,
    GENERIC_CODE_INVALID_VALUE
} = require('./codes.ts')
var server = app.listen(port, () => {
        var host = server.address().address
        var port = server.address().port
        console.log("Example app listening at http://%s:%s", host, port)
    })

//Don t rename the variable in process because it will fuck up your environment
var reportOrder = jsonCopy(require('./mock-json/reportOrder.json'))
app.use(bodyParser.json())

app.get('/test', (req, res) => {
    console.log(req.body)
    res.status(200)
    res.send(json)
})

//POST
app.post('/getAmenities', (req, res) => {
    /*
    res.status(200)
    res.send(amenities)
    */
   res.status(200)
   var amenities = jsonCopy(require('./mock-json/amenities.json'))
   switch(req.body.credentialInfo.credential){
       case SCENARIOS.CREDENTIAL_2000: {
            //setting the stored value to 2000
            amenities.balances[0].amount = "2000"
            amenities.balances[0].allowToSpend = "2000"
            amenities.vouchers = []
            break;
        }
       case SCENARIOS.CREDENTIAL_2001: {
            //storred value to 2
            amenities.balances[0].amount = "2"
            amenities.balances[0].allowToSpend = "2"
            break;
        }
       case SCENARIOS.CREDENTIAL_2002: {
            // -                       (ammenities returned 606)
            amenities = jsonCopy(require('./mock-json-error/errorAmenities.json'))
            amenities.code = GENERIC_CODE_INVALID_CREDENTIALS
            amenities.error = "Invalid Credentials"
            break;
        }
        case SCENARIOS.CREDENTIAL_2003: {
            //ERROR 606 - INVALID CREDENTIALS
            amenities = jsonCopy(require('./mock-json-error/errorAmenities.json'))
            amenities.code = GENERIC_CODE_INVALID_CREDENTIALS
            amenities.error = "Invalid Credentials"
            break;
        }
        case SCENARIOS.CREDENTIAL_2004: {
            amenities.balances[0].amount = "0"
            amenities.balances[0].allowToSpend = "0"
            break;
        }
        case SCENARIOS.CREDENTIAL_2005: {
            amenities.balances[0].amount = "2000"
            amenities.balances[0].allowToSpend = "2000"
            amenities.vouchers = []
            break;
        }
        case SCENARIOS.CREDENTIAL_2006: {
            amenities = jsonCopy(require('./mock-json-error/errorAmenitiesActivate.json'))
            amenities.code = GENERIC_CODE_CARD_NOT_ACTIVE
            amenities.error = "Card not active"
            break;
        }
        case SCENARIOS.CREDENTIAL_2007: {
            amenities.balances[0].amount = "2"
            amenities.balances[0].allowToSpend = "2"
            amenities.vouchers = []
            break;
        }
        case SCENARIOS.CREDENTIAL_2008: {
            //setting the stored value to 2000
            amenities.balances[0].amount = "2"
            amenities.balances[0].allowToSpend = "2"
            amenities.vouchers = []
            break;
        }
        default: {
            amenities.error = "Default - you surpassed the covered credentials cases"
            break;
        }    
   }
   res.send(amenities)
})

//POST
app.post('/cashOut', (req, res) => {
    /*
    res.status(200)
    res.send(amenities)
    */
   res.status(200)
   var cashout = jsonCopy(require('./mock-json/cashout.json'))
   switch(req.body.credentialInfo.credential){
       case SCENARIOS.CREDENTIAL_2000: {
            return res.send({})
        }
       case SCENARIOS.CREDENTIAL_2001: {
           //set amount for cashOut
            cashout.code = 200
            cashout.amount = req.body.amount
            break;
        }
       case SCENARIOS.CREDENTIAL_2002: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2003: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2004: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2005: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2006: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2007: {
            //          error - 607 (INVALID VALUE)
            cashout = jsonCopy(require('./mock-json-error/errorCashOut.json'))
            cashout.code = GENERIC_CODE_INVALID_VALUE
            cashout.error = "Invalid Value"
            break;
        }
        case SCENARIOS.CREDENTIAL_2008: {
            cashout.code = 200
            cashout.amount = req.body.amount
            break;
        }
        default: {
            cashout.error = "Default - you surpassed the covered credentials cases"
            break;
        }    
   }
   res.send(cashout)
})

//POST
app.post('/process', (req, res) => {
    /*
    var processJson = jsonCopy(require('./mock-json/process.json'))
    if(req.body.credentialInfo.credential == VALIDATE_AGAINST || req.body.credentialInfo.credential === INVALID){
        var errorProcess = require('./mock-json-error/errorProcess.json')
        res.status(200)
        res.statusMessage = 'For providers that do not want to support partial payment of total (ordinarily process) response would come back with remaining > 0 and some payments/discounts)'
        return res.send(errorProcess)
    }
    processJson.payments[0].spent = req.body.amount
    res.status(200)
    res.send(processJson)*/
    
    console.log("aici0")
    var processJson = jsonCopy(require('./mock-json/process.json'))
    console.log("aici")
    switch(req.body.credentialInfo.credential){
        case SCENARIOS.CREDENTIAL_2000: {
            //OK full payment no discounts
            processJson.discounts = []
            processJson.remainingAmount = "0"
            processJson.payments[0].spent = req.body.amount
            break;
        }
        case SCENARIOS.CREDENTIAL_2001: {
             //OK - partial payment 1$ remain - 1 discount 10% off 1'st item
             //spentul se pune 0 daca platesc 5 dolari la bounty

             //scad ce primesc din getDiscounts pe server
            processJson.remainingAmount = "1"
            

            processJson.discounts[0].sku = req.body.orderInfo.cart[0].sku //ok
            processJson.discounts[0].name = req.body.orderInfo.cart[0].itemName + " discount" // numele de item discount care vine de pe serverul meu si asa il pun eu
            processJson.discounts[0].amount = req.body.orderInfo.cart[0].itemCost * 0.1 // cantitatea de discount pe care vrea sa o pun server side
            //problema este ca atunci cand eu fac un process pun in cart si item level discounts(astea ar fi reductionurile) pe care le am facut eu 
            //dar pe partea de server aplic doar discounts pe care vreau eu sa le aplic pe server -> un zece la suta (getDiscounts)
            processJson.payments[0].spent = (Number(req.body.amount) - 1) - processJson.discounts[0].amount +  "" //1 is the remaining amount
            break;
        }
        case SCENARIOS.CREDENTIAL_2002: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2003: {
            processJson = require('./mock-json-error/errorProcess.json')
            processJson.code = GENERIC_CODE_INVALID_CREDENTIALS
            processJson.error = "Invalid Credentials"
            break;
        }
        case SCENARIOS.CREDENTIAL_2004: {
            processJson = require('./mock-json-error/errorProcess.json')
            processJson.code = GENERIC_CODE_INSUFFICIENT_FUNDS
            processJson.error = "Insufficient funds"
            break;
        }
        //aici nu este corect
        //spent trebuie sa fie anmount din process
        case SCENARIOS.CREDENTIAL_2005: {
            //OK full payment no discounts
            processJson.discounts = []
            processJson.remainingAmount = "0" 
            processJson.payments[0].spent = req.body.amount
            break;
        }
        case SCENARIOS.CREDENTIAL_2006: {
            processJson = require('./mock-json-error/errorProcess.json')
            processJson.code = GENERIC_CODE_CARD_NOT_ACTIVE
            processJson.error = "Card not active"
            break;
        }
        case SCENARIOS.CREDENTIAL_2007: {
             //OK - partial payment 1$ remain - 1 discount 10% off 1'st item
             processJson.remainingAmount = "1"
            

             processJson.discounts[0].sku = req.body.orderInfo.cart[0].sku //ok
             processJson.discounts[0].name = req.body.orderInfo.cart[0].itemName + " discount" // numele de item name din server
             processJson.discounts[0].amount = req.body.orderInfo.cart[0].itemCost * 0.1 // numele de discount 
             processJson.payments[0].spent = (Number(req.body.amount) - 1) - processJson.discounts[0].amount +  "" //1 is the remaining amount
            break;
        }
        case SCENARIOS.CREDENTIAL_2008: {
            //this scenario is for low payment purpose
            //for example if i have a 20 dollar bounty to pay and i want to pay with 1 dollar and I have a 2 dollar discount
            //i will have a remaining amount of 0, a 0 spent 
            processJson.remainingAmount = "0"
           
            processJson.discounts[0].sku = req.body.orderInfo.cart[0].sku //ok
            processJson.discounts[0].name = req.body.orderInfo.cart[0].itemName + " discount" // numele de item name din server
            var appliedDiscount = req.body.orderInfo.cart[0].itemCost * 0.1
            if(appliedDiscount >= req.body.amount) {
                processJson.payments[0].spent = 0
                processJson.discounts[0].amount = req.body.amount  //daca amount este mai mic decat discount, tot ce am putut sa aplic este amount

            } else {
                processJson.payments[0].spent = Number(req.body.amount)  - appliedDiscount +  "" 
                processJson.discounts[0].amount = appliedDiscount

            }
           break;
       }
        default: {
            processJson.error = "Default - you surpassed the covered credentials cases"
            break;
        }    
    }
    res.send(processJson)
})
//pentru un card o sa dea doar un reduction ->red
//pentru alt card reduction si discount     ->reddis
//pentru alt card o sa dea doar un discount ->dis
//pe langa success si failure
//mock_1234_get_benefits
//code 200
//POST

// VOID PAYMENT
app.post('/void', (req, res) => {
    /*
    if(req.body.externalId === VALIDATE_AGAINST || req.body.externalId === INVALID){
        var errorVoid1 = require('./mock-json-error/errorVoid1.json')
        res.status(200)
        res.statusMessage = 'When trying to refund a transaction and the account was removed (like cashed out)'
        return res.send(errorVoid1)
    } 
    else if(req.body.externalId === processJson.payments[0].externalId){
        res.status(200)
        return res.send(voidJson)
    }
    else if(req.body.externalId !== processJson.payments[0].externalId){
        var errorVoid2 = require('./mock-json-error/errorVoid2.json')
        res.status(200)
        res.statusMessage = 'When refunding a transaction that was already refunded'
        return res.send(errorVoid2)
    }*/
    var voidJson = jsonCopy(require('./mock-json/void.json'))
    voidJson.code = 200
    switch(LAST_CREDENTIAL){
        case SCENARIOS.CREDENTIAL_2000: {
            voidJson.externalId = req.body.externalId
            break;
        }
        case SCENARIOS.CREDENTIAL_2001: {
            voidJson.externalId = req.body.externalId
            break;
        }
        case SCENARIOS.CREDENTIAL_2002: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2003: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2004: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2005: {
            voidJson = require('./mock-json-error/errorVoid2.json')
            voidJson.code = GENERIC_CODE_ALREADY_REFUNDED
            voidJson.error = "Already refunded"
            break;
        }
        case SCENARIOS.CREDENTIAL_2006: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2007: {
            voidJson = require('./mock-json-error/errorVoid1.json')
            voidJson.code = GENERIC_CODE_NO_VALUE
            voidJson.error = "No value"
            break;
        }
        case SCENARIOS.CREDENTIAL_2008: {
            voidJson.externalId = req.body.externalId
            break;
        }
        default: {
            res.send({"default": "you surpassed the covered credentials cases"})
            break;
        }    
    }
    res.send(voidJson)
    
})

app.post('/activate', (req, res) => {
    /*
    if(req.body.credentialInfo.credential === VALIDATE_AGAINST || req.body.credentialInfo.credential === INVALID){
        var errorActivate = require('./mock-json-error/errorActivate.json')
        res.status(200)
        res.statusMessage = 'When trying to activate a card that was already activated'
        return res.send(errorActivate)
    }
    res.status(200)
    res.send(activate)*/
    var activate = jsonCopy(require('./mock-json/activate.json'))
    res.status(200)
    switch(req.body.credentialInfo.credential){
        case SCENARIOS.CREDENTIAL_2000: {
            activate = require('./mock-json-error/errorActivate.json')
            activate.code = GENERIC_CODE_CARD_ALREADY_ACTIVE
            activate.error = "Card already active"
            break;
        }
        case SCENARIOS.CREDENTIAL_2001: {
            activate = require('./mock-json-error/errorActivate.json')
            activate.code = GENERIC_CODE_CARD_ALREADY_ACTIVE
            activate.error = "Card already active"
            break;
        }
        case SCENARIOS.CREDENTIAL_2002: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2003: {
            activate = require('./mock-json-error/errorActivate.json')
            activate.code = GENERIC_CODE_INVALID_CREDENTIALS
            activate.error = "Invalid Credentials"
            break;
        }
        case SCENARIOS.CREDENTIAL_2004: {
            activate = require('./mock-json-error/errorActivate.json')
            activate.code = GENERIC_CODE_CARD_ALREADY_ACTIVE
            activate.error = "Card already active"
            break;
        }
        case SCENARIOS.CREDENTIAL_2005: {
            activate = require('./mock-json-error/errorActivate.json')
            activate.code = GENERIC_CODE_CARD_ALREADY_ACTIVE
            activate.error = "Card already active"
            break;
        }
        case SCENARIOS.CREDENTIAL_2006: {
             //OK
            break;
        }
        case SCENARIOS.CREDENTIAL_2007: {
            activate = require('./mock-json-error/errorActivate.json')
            activate.code = GENERIC_CODE_INVALID_CREDENTIALS
            activate.error = "Invalid Credentials"
            break;
        }
        case SCENARIOS.CREDENTIAL_2008: {
            activate = require('./mock-json-error/errorActivate.json')
            activate.code = GENERIC_CODE_CARD_ALREADY_ACTIVE
            activate.error = "Card already active"
            break;
        }
        default: {
            activate.error = "Default - you surpassed the covered credentials cases"
            break;
        }    
    }
    res.send(activate)
})

app.post('/reload', (req, res) => {
    /*
    if(req.body.credentialInfo.credential === VALIDATE_AGAINST || req.body.credentialInfo.credential === INVALID){
        var errorReload = require('./mock-json-error/errorReload.json')
        res.status(200)
        res.statusMessage = 'When trying to reload a card that has not yet been activated or was deactivated'
        return res.send(errorReload)
    }
    res.status(200)
    res.send(reload)*/
    var reload = jsonCopy(require('./mock-json/reload.json'))
    res.status(200)
    switch(req.body.credentialInfo.credential){
        case SCENARIOS.CREDENTIAL_2000: {
             //OK
            break;
        }
        case SCENARIOS.CREDENTIAL_2001: {
             //OK
            break;
        }
        case SCENARIOS.CREDENTIAL_2002: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2003: {
            reload = require('./mock-json-error/errorReload.json')
            reload.code = GENERIC_CODE_INVALID_CREDENTIALS
            reload.error = "Invalid Credentials"
            break;
        }
        case SCENARIOS.CREDENTIAL_2004: {
            //OK
            break;
        }
        case SCENARIOS.CREDENTIAL_2005: {
            //OK
            break;
        }
        case SCENARIOS.CREDENTIAL_2006: {
            reload = require('./mock-json-error/errorReload.json')
            reload.code = GENERIC_CODE_CARD_NOT_ACTIVE
            reload.error = "Card not active"
            break;
        }
        case SCENARIOS.CREDENTIAL_2007: {
            reload = require('./mock-json-error/errorReload.json')
            reload.code = GENERIC_CODE_INVALID_CREDENTIALS
            reload.error = "Invalid Credentials"
            break;
        }
        case SCENARIOS.CREDENTIAL_2008: {
            //OK
           break;
       }
        default: {
            reload.error = "Default - you surpassed the covered credentials cases"
            break;
        }    
    }
    res.send(reload)
})

app.post('/getReductions', (req, res) => {
    /*
    var reductions = jsonCopy(require('./mock-json/getReductions.json'))
    console.log(reductions)

    switch(req.body.credentialInfo.credential) {
        case dis: {
            reductions.reductions[0] = []
            res.status(200)
            res.send(reductions)
            break
        }
        case red: 
        case reddis: {
            console.log(req.body.credentialInfo.credential)
            res.status(200)
            res.send(reductions)
            break
        }
        default: {
            console.log("here")
            reductions.reductions[0].reductionId = 0
            reductions.reductions[0].remoteId = "default"
            res.status(200)
            res.send(reductions)
        }
    }*/
    var reductions = jsonCopy(require('./mock-json/getReductions.json'))
    switch(req.body.credentialInfo.credential){
        case SCENARIOS.CREDENTIAL_2000: {
            reductions.reductions = []
            break;
         }
        case SCENARIOS.CREDENTIAL_2001: {
            reductions.reductions[0].reductionId = 380
            reductions.reductions[0].reductionId = "380"
            break;
         }
        case SCENARIOS.CREDENTIAL_2002: {
            return res.send({})
         }
         case SCENARIOS.CREDENTIAL_2003: {
            reductions = require('./mock-json-error/errorGetReductions.json')
            reductions.code = GENERIC_CODE_INVALID_CREDENTIALS
            reductions.error = "Invalid Credentials"
            break;
         }
         case SCENARIOS.CREDENTIAL_2004: {
            reductions.reductions[0].reductionId = 380
            reductions.reductions[0].reductionId = "380"
            break;
         }
         case SCENARIOS.CREDENTIAL_2005: {
            reductions.reductions = []
            break;
         }
         case SCENARIOS.CREDENTIAL_2006: {
            reductions = require('./mock-json-error/errorGetReductions.json')
            reductions.code = GENERIC_CODE_CARD_NOT_ACTIVE
            reductions.error = "Card not active"
            break;
         }
         case SCENARIOS.CREDENTIAL_2007: {
            reductions.reductions = []
            break;
         }
         case SCENARIOS.CREDENTIAL_2008: {
            reductions.reductions = []
            break;
         }
         default: {
            reductions.error = "Default - you surpassed the covered credentials cases"
            break;
         }    
    }
    res.status(200)
    res.send(reductions)
})

app.post('/getDiscounts', (req, res) => {
    /*
    var discounts = jsonCopy(require('./mock-json/getDiscounts.json'))
    switch(req.body.credentialInfo.credential) {
        case red: {
            discounts.discounts = []
            res.status(200)
            res.send(discounts)
            break
        }
        case dis:
        case reddis: {
            discounts.discounts[0].sku = req.body.orderInfo.cart[0].sku
            discounts.discounts[0].name = req.body.orderInfo.cart[0].itemName
            discounts.discounts[0].amount = 0.5
            res.status(200)
            res.send(discounts)
            break
        }
        default: {
            res.status(200)
            res.send(discounts)
        }
    }*/
    res.status(200)
    var discounts = jsonCopy(require('./mock-json/getDiscounts.json'))
    switch(req.body.credentialInfo.credential){
        case SCENARIOS.CREDENTIAL_2000: {
             //ok- no discounts
            discounts.discounts = []
            break;
        }
        case SCENARIOS.CREDENTIAL_2001: {
            discounts.discounts[0].sku = req.body.orderInfo.cart[0].sku
            discounts.discounts[0].name = req.body.orderInfo.cart[0].itemName
            discounts.discounts[0].amount = req.body.orderInfo.cart[0].itemCost * 0.1
            break;
        }
        case SCENARIOS.CREDENTIAL_2002: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2003: {
            discounts = require('./mock-json-error/errorGetDiscounts.json')
            discounts.code = GENERIC_CODE_INVALID_CREDENTIALS
            discounts.error = "Invalid Credentials"
            break;
        }
        case SCENARIOS.CREDENTIAL_2004: {
            discounts.discounts[0].sku = req.body.orderInfo.cart[0].sku
            discounts.discounts[0].name = req.body.orderInfo.cart[0].itemName
            discounts.discounts[0].amount = req.body.orderInfo.cart[0].itemCost * 0.1
            break;
        }
        case SCENARIOS.CREDENTIAL_2005: {
            //ok- no discounts
            discounts.discounts = []
            break;
        }
        case SCENARIOS.CREDENTIAL_2006: {
            discounts = require('./mock-json-error/errorGetDiscounts.json')
            discounts.code = GENERIC_CODE_CARD_NOT_ACTIVE
            discounts.error = "Card not active"
            break;
        }
        case SCENARIOS.CREDENTIAL_2007: {
            //ok- no discounts
            discounts.discounts = []
            break;
        }
        case SCENARIOS.CREDENTIAL_2008: {
            //discounts.discounts = []
            discounts.discounts[0].sku = req.body.orderInfo.cart[0].sku
            discounts.discounts[0].name = req.body.orderInfo.cart[0].itemName
            discounts.discounts[0].amount = req.body.orderInfo.cart[0].itemCost * 0.1
            break;
        }
        default: {
            discounts.error = "Default - you surpassed the covered credentials cases"
            break;
        }    
    }
    res.send(discounts)
})

app.post('/useReductions', (req, res) => {
    /*
    useReductions.reductions = req.body.reductions
    res.status(200)
    res.send(useReductions)*/
    var useReductions = jsonCopy(require('./mock-json/useReductions.json'))
    switch(req.body.credentialInfo.credential){
        case SCENARIOS.CREDENTIAL_2000: {
            return res.send({}) 
        }
        case SCENARIOS.CREDENTIAL_2001: {
            //OK
            useReductions.reductions = req.body.reductions
            break;
         }
        case SCENARIOS.CREDENTIAL_2002: {
            return res.send({}) 
        }
         case SCENARIOS.CREDENTIAL_2003: {
            return res.send({}) 
        }
         case SCENARIOS.CREDENTIAL_2004: {
             //OK
            useReductions.reductions = req.body.reductions
            break;
         }
         case SCENARIOS.CREDENTIAL_2005: {
            return res.send({}) 
        }
         case SCENARIOS.CREDENTIAL_2006: {
            return res.send({}) 
        }
         case SCENARIOS.CREDENTIAL_2007: {
            return res.send({}) 
         }
         case SCENARIOS.CREDENTIAL_2008: {
            return res.send({}) 
         }
         default: {
            useReductions.error = "Default - you surpassed the covered credentials cases"
            break;
         }    
    }
    res.status(200)
    res.send(useReductions) 
})

app.post('/voidReductions', (req, res) => {
    /*
    voidReductions.reductions = req.body.reductions
    res.status(200)
    res.send(voidReductions)
    */
   var voidReductions = jsonCopy(require('./mock-json/voidReductions.json'))
   res.status(200)
   switch(req.body.credentialInfo.credential){
    case SCENARIOS.CREDENTIAL_2000: {
        return res.send({})
    }
    case SCENARIOS.CREDENTIAL_2001: {
        voidReductions.reductions = req.body.reductions
        break;
    }
    case SCENARIOS.CREDENTIAL_2002: {
        return res.send({})
    }
    case SCENARIOS.CREDENTIAL_2003: {
        return res.send({})
    }
    case SCENARIOS.CREDENTIAL_2004: {
        voidReductions.reductions = req.body.reductions
        break;
    }
    case SCENARIOS.CREDENTIAL_2005: {
        return res.send({})     
    }
    case SCENARIOS.CREDENTIAL_2006: {
        return res.send({})     
    }
    case SCENARIOS.CREDENTIAL_2007: {
        return res.send({})     
    }
    case SCENARIOS.CREDENTIAL_2008: {
        return res.send({})     
    }
    default: {
        voidReductions.error = "Default - you surpassed the covered credentials cases"
        break;
    }    
}
res.send(voidReductions)
})

app.post('/reportOrder', (req, res) => {
    res.status(200)
    console.log(LAST_CREDENTIAL)
    switch(LAST_CREDENTIAL){
        case SCENARIOS.CREDENTIAL_2000: {
            return res.send(reportOrder)
        }
        case SCENARIOS.CREDENTIAL_2001: {
            return res.send(reportOrder)
        }
        case SCENARIOS.CREDENTIAL_2002: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2003: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2004: {
            return res.send({})
        }
        case SCENARIOS.CREDENTIAL_2005: {
            return res.send(reportOrder) 
        }
        case SCENARIOS.CREDENTIAL_2006: {
            return res.send({}) 
        }
        case SCENARIOS.CREDENTIAL_2007: {
            return res.send(reportOrder) 
        }
        case SCENARIOS.CREDENTIAL_2008: {
            return res.send(reportOrder) 
        }
        default: {
            reportOrder.error = "Default - you surpassed the covered credentials cases"
            return res.send(reportOrder)            
        }    
    }
})

app.post('/lookup', (req, res) => {
    /*
    console.log(VALIDATE_AGAINST)
    if(req.body.userInfo.phoneNumber === VALIDATE_AGAINST || req.body.userInfo.phoneNumber === INVALID){
        var errorLookup = require('./mock-json-error/errorLookup.json')
        res.status(200)
        res.statusMessage = 'Emitted by lookup if the phone/email/fullName does not match any account or if it matches multiple accounts'
        return res.send(errorLookup)
    }
    res.status(200)
    res.send(lookup)
    */
   res.status(200)
   var lookup = jsonCopy(require('./mock-json/lookup.json'))
   switch(req.body.userInfo.phoneNumber){
       case SCENARIOS.PHONE_1000: {
            LAST_CREDENTIAL = SCENARIOS.CREDENTIAL_2000
            lookup.accountId = SCENARIOS.CREDENTIAL_2000
            break;
        }
       case SCENARIOS.PHONE_1001: {
            LAST_CREDENTIAL = SCENARIOS.CREDENTIAL_2001
            lookup.accountId = SCENARIOS.CREDENTIAL_2001
            break;
        }
       case SCENARIOS.PHONE_1002: {
            LAST_CREDENTIAL = SCENARIOS.CREDENTIAL_2002  
            lookup = require('./mock-json-error/errorLookup.json')
            break;
        }
        case SCENARIOS.PHONE_1003: {
            LAST_CREDENTIAL = SCENARIOS.CREDENTIAL_2003
            lookup.accountId = SCENARIOS.CREDENTIAL_2003
            break;
        }
        case SCENARIOS.PHONE_1004: {
            LAST_CREDENTIAL = SCENARIOS.CREDENTIAL_2004
            lookup.accountId = SCENARIOS.CREDENTIAL_2004
            break;
        }
        case SCENARIOS.PHONE_1005: {
            LAST_CREDENTIAL = SCENARIOS.CREDENTIAL_2005
            lookup.accountId = SCENARIOS.CREDENTIAL_2005
            break;
        }
        case SCENARIOS.PHONE_1006: {
            LAST_CREDENTIAL = SCENARIOS.CREDENTIAL_2006
            lookup.accountId = SCENARIOS.CREDENTIAL_2006
            break;
        }
        case SCENARIOS.PHONE_1007: {
            LAST_CREDENTIAL = SCENARIOS.CREDENTIAL_2007
            lookup.accountId = SCENARIOS.CREDENTIAL_2007
            break;
        }
        case SCENARIOS.PHONE_1008: {
            LAST_CREDENTIAL = SCENARIOS.CREDENTIAL_2008
            lookup.accountId = SCENARIOS.CREDENTIAL_2008
            break;
        }
        default: {
            LAST_CREDENTIAL = "default"
            lookup.accountId = "default"
            break;
        }    
   }
   res.send(lookup)
})

app.get('/ping', (req, res) => {
    res.status(200)
   res.send()
})

function jsonCopy(src) {
    return JSON.parse(JSON.stringify(src));
}

var json = {
    "success": true,
    "message": "gg boss",
    "description": "description"
}

/*
switch(req.body.credentialInfo.credential){
        case SCENARIOS.CREDENTIAL_2000: {
             
            break;
        }
        case SCENARIOS.CREDENTIAL_2001: {
             
            break;
        }
        case SCENARIOS.CREDENTIAL_2002: {
            
            break;
        }
        case SCENARIOS.CREDENTIAL_2003: {
            
            break;
        }
        case SCENARIOS.CREDENTIAL_2004: {
             
            break;
        }
        case SCENARIOS.CREDENTIAL_2005: {
             
            break;
        }
        case SCENARIOS.CREDENTIAL_2006: {
             
             break;
        }
        case SCENARIOS.CREDENTIAL_2007: {
             
             break;
        }
        default: {
             
             break;
        }    
    }
*/
//CASES: https://docs.google.com/spreadsheets/d/1GBzpiBDzTOhnJ37eL5Jb4elOg8XAkKmoc0agv0ut5ZU/edit#gid=1098935911